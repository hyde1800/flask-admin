from manage import *

class Base_adminUser(db.Model):
    __tablename__ = 'Base_admin_user'
    user_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    password = db.Column(db.String(128))
    email = db.Column(db.String(128), unique=True)
    department = db.Column(db.String(128),nullable=True)
    position = db.Column(db.String(128),nullable=True)
    role_id = db.Column(db.Integer,nullable=True)
    time = db.Column(db.DateTime)
    status = db.Column(db.Integer,default=0)

    def __init__(self, name,password,email,department,position,role_id,time,status):
        self.name = name
        self.password = password
        self.email = email
        self.department = department
        self.position = position
        self.role_id = role_id
        self.time = time
        self.status = status


