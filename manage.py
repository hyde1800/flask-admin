from flask import Flask
from app.role import role
from app.user import user
from flask_sqlalchemy import SQLAlchemy
from config import *


apple=Flask(__name__)

apple.config['SQLALCHEMY_DATABASE_URI'] = DB_CONNECT
apple.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(apple)

apple.register_blueprint(user,url_prefix='/user')      #注册user蓝图，没有指定前缀。
apple.register_blueprint(role,url_prefix='/role')    #注册role蓝图，并指定前缀。


if __name__=='__main__':
         apple.run(host='0.0.0.0',port=5000,debug=True)  #运行flask http程序，host指定监听IP，port指定监听端口，调试时需要开启debug模式。


