from flask import Flask, jsonify
from app.user import user
from manage import db
from config import *
from flask_restful import Resource, Api
import time
from models.user import Base_adminUser
import json

app = user
api = Api(app)

'''
    新增加用户模块
'''
class AddUser(Resource):
    def post(self):
        #先获取先端信息
        #format_data = json.loads(self.request.body)
        #user_name = 'zhangye'
        user_name = self.get_argument('name')
        #email = '1@qq.com'
        email = self.get_argument('email')
        passWord = '123456'
        department = ''
        position = ''
        role_id = 0
        creat_time = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time.time()))
        status = 0

        user_data = Base_adminUser(user_name,passWord,email,department,position,role_id,creat_time,status)
        #数据库操作
        try:
            db.session.add(user_data)
            db.session.commit()
            db.session.close()
        except Exception as e:
            print (e)
            return jsonify({'status': 0})

        return jsonify({'status':1})


'''
    删除用户（软删除）
'''
class DelUser(Resource):
    def post(self):
        #通过email唯一标识用户
        format_data = json.loads(self.request.body)
        email = format_data.get('email','')

        #数据库操作
        try:
            db.session.query(Base_adminUser).filter(Base_adminUser.email == email).update({Base_adminUser.status: '2'})
            db.session.commit()
            db.session.close()
        except Exception as e:
            print (e)
            return jsonify({'status': 0})

        return jsonify({'status': 1})

'''
    停用用户
'''
class DisableUser(Resource):
    def post(self):
        # 通过email唯一标识用户
        format_data = json.loads(self.request.body)
        email = format_data.get('email', '')

        # 数据库操作
        try:
            db.session.query(Base_adminUser).filter(Base_adminUser.email == email).update({Base_adminUser.status: '1'})
            db.session.commit()
            db.session.close()
        except Exception as e:
            print(e)
            return jsonify({'status': 0})

        return jsonify({'status': 1})

'''
    修改更新用户
'''
class ModifyUser(Resource):
    def post(self):
        # 通过email唯一标识用户
        format_data = json.loads(self.request.body)
        email = format_data.get('email', '')
        department = format_data.get('department', '')
        position = format_data.get('position', '')

        # 数据库操作
        try:
            db.session.query(Base_adminUser).filter(Base_adminUser.email == email).update({Base_adminUser.department: department,Base_adminUser.position: position})
            db.session.commit()
            db.session.close()
        except Exception as e:
            print(e)
            return jsonify({'status': 0})

        return jsonify({'status': 1})




#restful API 路由
api.add_resource(AddUser, '/addUser')
api.add_resource(DelUser, '/delUser')
api.add_resource(DisableUser, '/disableUser')
api.add_resource(ModifyUser, '/modifyUser')