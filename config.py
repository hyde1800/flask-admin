DEBUG = True
TEST_DEBUG = True


if DEBUG:
    from local_config import *

if not DEBUG:
    from server_config import *
    if TEST_DEBUG:
        DB_CONNECT = DB_CONNECT_TEST
    else:
        DB_CONNECT =DB_CONNECT

